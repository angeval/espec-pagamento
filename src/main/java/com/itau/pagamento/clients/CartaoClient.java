package com.itau.pagamento.clients;

import com.itau.pagamento.models.dtos.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {
    @GetMapping("/cartao/id/{id}")
    Cartao findById(@PathVariable int id);

    @GetMapping("/{numeroCartao}")
    Cartao findByNumero(@PathVariable String numeroCartao);
}