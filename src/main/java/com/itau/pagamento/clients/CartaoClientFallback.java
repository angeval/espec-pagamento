package com.itau.pagamento.clients;

import com.itau.pagamento.exceptions.CartaoNotFoundException;
import com.itau.pagamento.models.dtos.Cartao;
import com.netflix.client.ClientException;

import java.io.IOException;

public class CartaoClientFallback implements CartaoClient {

    private Exception cause;

    CartaoClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public Cartao findById(int id) {
        if (cause instanceof ClientException) {
            throw new RuntimeException("O serviço de cartão está offline");
        }

        if (cause instanceof IOException) {
            throw new RuntimeException("O serviço de cartão está fora do ar");
        }
        Cartao cartao = new Cartao();
        if (cause instanceof CartaoNotFoundException) {
            throw new CartaoNotFoundException();
        }
        // cartao fake
        //o correto seria fazer um log e disparar alguma fila para refazer o cliente quando o serviço voltasse para o ar
        cartao.setId(-1);
        cartao.setAtivo(false);
        cartao.setNumero("9999999999999");
        cartao.setClienteId(-1);
        return cartao;
    }

    @Override
    public Cartao findByNumero(String numeroCartao) {
        if (cause instanceof RuntimeException) {
            throw new RuntimeException("O serviço de cartão está offline");
        }

        if (cause instanceof IOException) {
            throw new RuntimeException("O serviço de cartão está fora do ar");
        }

        throw (RuntimeException) cause;
        // cartao fake
        //o correto seria fazer um log e disparar alguma fila para refazer o cliente quando o serviço voltasse para o ar
        //Cartao cartao = new Cartao();
        //cartao.setId(-1);
        //cartao.setAtivo(false);
        //cartao.setNumero("9999999999999");
        //cartao.setClienteId(-1);
        //return cartao;
    }
}