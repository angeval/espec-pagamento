package com.itau.pagamento.services;

import com.itau.pagamento.clients.CartaoClient;
import com.itau.pagamento.exceptions.CartaoNotFoundException;
import com.itau.pagamento.models.Pagamento;
import com.itau.pagamento.models.dtos.Cartao;
import com.itau.pagamento.repositories.PagamentoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criarPagamento(Pagamento pagamento){
        Cartao cartao = new Cartao();

        try {
            cartao = cartaoClient.findById(pagamento.getCartaoId());
            return pagamentoRepository.save(pagamento);
        } catch (FeignException.NotFound e) {
            throw new CartaoNotFoundException();
        }
    }

    public List<Pagamento> buscarPorCartaoId(int cartaoId){
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }

}
