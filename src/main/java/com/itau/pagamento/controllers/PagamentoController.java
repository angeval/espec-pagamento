package com.itau.pagamento.controllers;

import com.itau.pagamento.clients.CartaoClient;
import com.itau.pagamento.models.Pagamento;
import com.itau.pagamento.models.dtos.CreatePagamentoRequest;
import com.itau.pagamento.models.dtos.PagamentoResponse;
import com.itau.pagamento.models.mapper.PagamentoMapper;
import com.itau.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoClient cartaoClient;

    @GetMapping("/pagamentos/{cartaoId}")
    public List<PagamentoResponse> buscarPorCartaoId(@PathVariable Integer cartaoId) {
        PagamentoMapper pagamentoMapper = new PagamentoMapper();
        List<Pagamento> pagamentos = pagamentoService.buscarPorCartaoId(cartaoId);
        return pagamentoMapper.toPagamentoResponse(pagamentos);
    }

    @PostMapping("/pagamento")
    public PagamentoResponse criarPagamento(@RequestBody @Valid CreatePagamentoRequest createPagamentoRequest) {
        PagamentoMapper pagamentoMapper = new PagamentoMapper();
        Pagamento pagamento = pagamentoMapper.toPagamento(createPagamentoRequest);
        pagamento = pagamentoService.criarPagamento(pagamento);
        return pagamentoMapper.toPagamentoResponse(pagamento);
    }

}