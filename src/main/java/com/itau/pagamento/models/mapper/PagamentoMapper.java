package com.itau.pagamento.models.mapper;

import com.itau.pagamento.clients.CartaoClient;
import com.itau.pagamento.exceptions.CartaoNotFoundException;
import com.itau.pagamento.models.Pagamento;
import com.itau.pagamento.models.dtos.Cartao;
import com.itau.pagamento.models.dtos.CreatePagamentoRequest;
import com.itau.pagamento.models.dtos.PagamentoResponse;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class PagamentoMapper {

    public PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(pagamento.getCartaoId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }

    public List<PagamentoResponse> toPagamentoResponse(List<Pagamento> pagamentos) {
        List<PagamentoResponse> pagamentoResponses = new ArrayList<>();

        for (Pagamento pagamento : pagamentos) {
            pagamentoResponses.add(toPagamentoResponse(pagamento));
        }

        return pagamentoResponses;
    }

    public Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());
        pagamento.setCartaoId(createPagamentoRequest.getCartaoId());

        return pagamento;
    }

}
